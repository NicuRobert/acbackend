const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/', express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER, varsta VARCHAR(3), CNP VARCHAR (20), sex VARCHAR(1), UNIQUE(email))";
    connection.query(sql, function (err, result) {
        if (err) throw err;
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.abonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let varsta = req.body.varsta;
    let CNP = req.body.CNP;
    let error = []



    if (!nume || !prenume || !telefon || !email || !facebook || !tipAbonament || !nrCard || !cvv || !varsta || !CNP) {
        error.push("Unul sau mai multe campuri nu au fost introduse!");
        console.log("Unul sau mai multe campuri nu au fost introduse!");
    }

    if (nume.length < 3 || nume.length > 20) {
        console.log("Nume invalid!");
        error.push("Nume invalid!");

    } else if (!nume.match("^[A-Za-z]+$")) {
        console.log("Numele trebuie sa contina doar litere!");
        error.push("Numele trebuie sa contina doar litere!");
    }

    if (prenume.length < 2 || prenume.length > 30) {
        console.log("Nume invalid!");
        error.push("Nume invalid");

    } else if (!prenume.match("^[A-Za-z]+$")) {
        console.log("Prenumele trebuie sa contina doar litere!");
        error.push("Prenumele trebuie sa contina doar litere!");
    }

    if (telefon.length != 10) {
        console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
        error.push("Numarul de telefon trebuie sa fie de 10 cifre!");

    } else if (!telefon.match("^[0-9]+$")) {
        console.log("Numarul de telefon trebuie sa contina doar cifre!");
        error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }

    if (!email.includes("@gmail.com") && !email.includes("@yahoo.com")) {
        console.log("Email invalid!");
        error.push("Email invalid!");
    }

    if (!facebook.includes('https://www.facebook.com/')) {
        console.log("Facebook invalid!");
        error.push("Facebook invalid!");
    }

    if (nrCard.length != 16 || !nrCard.match("^[0-9]+$")) {
        console.log("Card!");
        error.push("Card !");
    }

    if (cvv.length != 3 || !cvv.match("^[0-9]+$")) {
        console.log("CVV!");
        error.push("CVV!");
    }

    if (varsta.length < 1 || varsta.length > 3 || !varsta.match("^[0-9]+$")) {
        console.log("Varsta invalida!");
        error.push("Varsta invalida!");
    }

    if (CNP.length != 13 || !CNP.match("^[0-9]+$")) {
        console.log("CNP invalid!");
        error.push("CNP invalid!");
    }

    let an;
    let luna = CNP.substring(3, 5);
    let zi = CNP.substring(5, 7);

    if (CNP[0] == '1' || CNP[0] == '2') {
        an = 1900 + CNP[1] * 10 + CNP[2] * 1;
    }
    else if (CNP[0] == '5' || CNP[0] == '6') {
        an = 2000 + CNP[1] * 10 + CNP[2] * 1;
    }
    else if (CNP[0] == '3' || CNP[0] == '4') {
        an = 1800 + CNP[1] * 10 + CNP[2] * 1;
    }


    let azi = new Date()
    let varstaActuala = azi.getFullYear() - an;
    let lunaActuala = azi.getMonth() - luna + 1;
    let ziActuala = azi.getDate() - zi;


    if (lunaActuala < 0 || (lunaActuala == 0 && ziActuala < zi))
        varstaActuala--;

    if (varstaActuala != varsta) {
        console.log("Varsta si CNP-ul nu corespund!");
        error.push("Varsta si CNP-ul nu corespund!");
    }


    if (CNP[0] == 1 || CNP[0] == 3 || CNP[5] == 5) sex = "M";
    else if (CNP[0] == 2 || CNP[0] == 4 || CNP[5] == 6) sex = "F";
    else if (CNP[0] == 0 || CNP[0] == 9) error.push("CNP invalid!");

    if (error.length === 0) {
        const sql =
            "INSERT INTO abonament (nume,prenume,telefon,email,facebook,tipAbonament,nrCard,cvv,varsta,CNP,sex) VALUES('" + nume + "','" + prenume + "','" + telefon + "','" + email + "','" + facebook + "','" + tipAbonament + "','" + nrCard + "','" + cvv + "','" + varsta + "','" + CNP + "', '" + sex + "')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Abonament achizitionat!");
        });

        res.status(200).send({
            message: "Abonament achizitionat!"
        });
        console.log(sql);
    } else {
        res.status(500).send(error)
        console.log("Eroare la inserarea in baza de date!");
    }
});